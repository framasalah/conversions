#!/bin/bash

dir_path=$(dirname $(realpath $0));


args=('pdf_to_gray_pdf' 'png_to_heatmap_png' 'png_to_txt' 'swap_png_jpg' 'pdf_to_pdf_with_watermark' 'jpg_to_scanned_pdf_document')
app_names=('pdf_to_gray_pdf' 'png_to_heatmap_png' 'png_to_txt' 'swap_png_jpg' 'pdf_to_pdf_with_watermark' 'jpg_to_scanned_pdf_document')
icons=('pdf_to_gray_pdf' 'png_to_heatmap_png' 'png_to_txt' 'swap_png_jpg' 'pdf_to_pdf_with_watermark' 'jpg_to_scanned_pdf_document')


for i in "${!args[@]}"; do
    short_cut_path="$dir_path/_${app_names[i]}.desktop"
    rm -f $short_cut_path;
    touch $short_cut_path;
    chmod +x $short_cut_path
    echo "[Desktop Entry]" >> $short_cut_path
    echo "Comment=Conversions" >> $short_cut_path
    echo "Exec=bash -c 'cd $dir_path && python3 -m conversions_code ${args[i]}'" >> $short_cut_path
    echo "Icon=$dir_path/conversions_code/resources/${icons[i]}.png" >> $short_cut_path
    echo "Name=Conversions" >> $short_cut_path
    echo "StartupNotify=true" >> $short_cut_path
    echo "Terminal=true" >> $short_cut_path
    echo "Type=Application" >> $short_cut_path
    echo "Version=1.0" >> $short_cut_path
done


echo ""
echo "Conversions applications shortcuts established successfully"
echo "Path=$dir_path"
echo "Remember to rerun create_apps_shortcuts.sh if you change conversions folder path"
echo ""
