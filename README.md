# What is this app for?

Conversions contains multiple applications for **Ubuntu** that aims for facilitating batch conversion of multiple files. 

# Installation

## Fast method

All dependencies must be installed before launching the application. This can be
done by launching :

```
bash install.sh
``` 

## Detailed method

Otherwise, each dependency can be installed individually as presented bellow:

#### 1. Zenity

Used for executing of GTK+ dialog boxes for file path choosing. install by command:

```
sudo apt-get install zenity
```

#### 2. Tkinter python module

Used for GUI manipulation, installed by command:

```
sudo apt-get install python3-tk
```


#### 3. Tesseract python module
Used for text recognition, install by command (with multiple languages):

```
sudo apt-get install tesseract-ocr
sudo apt-get install tesseract-ocr-ara
sudo apt-get install tesseract-ocr-deu
sudo apt-get install tesseract-ocr-fra
sudo apt-get install tesseract-ocr-eng
sudo apt-get install tesseract-ocr-rus

```

#### 4. matplotlib

Used for image manipulation, installed by command:

```
sudo apt-get install python3-matplotlib
```

#### 5. Scipy

Used for special matrix calculation like convolution, installed by command:

```
sudo apt-get install python3-scipy
```

#### 6. Pillow

Used for image manipulation in Tkinter Gui, installed by command:

```
sudo apt-get -y install python3-pil
sudo apt-get -y install python3-pil.imagetk;
```

#### 7. pypdf2

Used for working with pdf files, installed by command:

```
sudo apt-get install python3-pypdf2
```

#### 8. Other dependencies

This program also uses ```ghostscript``` and ```ImageMagick (convert)```, which are installed by default in
ubuntu.

# How to use?

### Before running the apps


Before running any conversion program, the shortcuts must be made by running
 the following command line (in case install.sh was not run):

```
bash create_apps_shortcuts.sh
```

Changing Conversions folder path requires user to rerun  ```create_apps_shortcuts.sh``` 
in order to create a working shortcut.

### Explaining the applications

This project contains multiple applications, each one is used for a specific 
conversion task:

* **_pdf to gray pdf:** Used for converting colored pdf to gray pdf
(usually useful for printing without color).

* **_png to heatmap png:** Converts regular gray .png images to heat-maps, 
this is used usually in order to make depth maps captured by rgd-d cameras 
easier to understand

* **_png to txt:** Convert images to text using Tesseract, which is an open source
 text recognition engine (OCR).

* **_swap png jpg:** Converting .png image to .jpg ones,
and also convert .jpg (jpeg) to .png images.

* **_jpg to  scanned pdf document:** Transform jpg images captured by a camera to 
a scanned and digitized paper documents. Moreover, image processing parameters can be changed 
in [/conversions_code/config.py](conversions_code/config.py) under DOCUMENT\_SCANNER\_CONFIG variable.

* **_pdf to pdf with watermark:** Adds watermark to pdf files. the default base watermark document is placed
under [/support_files/watermark.pdf](support_files/watermark.pdf). Also we provide [watermark_changing.md](watermark_changing.md) in order to facilitate creating the user own watermark base document.

Each application can be run in two mode:

* **Mode 1:** In this mode, all files in the input folder will be processed and 
the result will be placed in the output folder. 

* **Mode 2:** In this mode, the user chooses the file(s) to Process using the gui.
 
# Customizing the Gui  
 
 Colors used in the gui are defined in config.py, as well as the window size.
