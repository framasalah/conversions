 #!/bin/bash
 
sudo apt-get -y install imagemagick;
 
sudo apt-get -y install zenity;

sudo apt-get -y install img2pdf;

sudo apt-get -y install python3-tk

sudo apt-get -y install python3-matplotlib;

sudo apt-get -y install python3-pypdf2;

sudo apt-get -y install python3-opencv;

sudo apt-get -y install python3-scipy;

sudo apt-get -y install python3-pil;

sudo apt-get -y install python3-pil.imagetk;


sudo apt-get -y install tesseract-ocr
sudo apt-get -y install tesseract-ocr-ara
sudo apt-get -y install tesseract-ocr-deu
sudo apt-get -y install tesseract-ocr-fra
sudo apt-get -y install tesseract-ocr-eng
sudo apt-get -y install tesseract-ocr-rus


echo ""
echo "Conversions dependencies installation finished successfully"


dir_path=$(dirname $(realpath $0));
bash "$dir_path/create_apps_shortcuts.sh";

