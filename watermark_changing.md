# Changing the default watermark

This is tutorial aims to show the user how to change the watermark placed in the background of pdf documents after using **jpg to  scanned pdf document** application.

By default using the application will produce the following watermark: *Change this watermark*.  

The user is urged to follow these steps in order to change the watermark:

### 1. Open LibreOffice Writer:

LibreOffice is an open source program which comes by default with ubuntu, in this tutorial we use LibreOffice writer which is a equivalent of Michrosoft Word.

### 2. Click on format

![help_watermark_1](conversions_code/resources/help_watermark_1.png)


### 3. Then click on watermark

![help_watermark_2](conversions_code/resources/help_watermark_2.png)

### 4. Type watermark text

The user can also change watermark size and color, we user the size of **85** and the color **Dark Gray 3**.

![help_watermark_3](conversions_code/resources/help_watermark_3.png)

### 5. Then click of file

![help_watermark_4](conversions_code/resources/help_watermark_4.png)

### 6. Then export as pdf

![help_watermark_5](conversions_code/resources/help_watermark_5.png)

### 7. Finally override the base watermark document

The base watermark document is located under [/support_files/watermark.pdf](support_files/watermark.pdf).
