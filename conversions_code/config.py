import os
import sys


def get_project_path():
    path = os.path.dirname(sys.modules['__main__'].__file__)
    return path[:-17]


GUI_CONFIG = {
    "title_font": ("Verdana", 42),
    "small_title_font": ("Verdana", 24),
    "base_font": ("Verdana", 14),
    "small_font": ("Verdana", 11),
    "size": (1200, 700),
    "enable_resize": False,
    "color1_foreground": "#284d00",
    "color1_background": "#e6ffcc",
    "color2_foreground": "#666600",
    "color2_background": "#ffffb3",
    "tooltip_foreground": "black",
    "tooltip_background": "#d6d9db",
    "red_color": "#b30000",
    "green_color": "#4f9900"
}

PATHS_CONFIG = {
    "input_folder": get_project_path() + "/input/",
    "output_folder": get_project_path() + "/output/",
    "watermark_pdf": get_project_path() + "/support_files/watermark.pdf",
}

DOCUMENT_SCANNER_CONFIG = {
    "dot_width_pixels": 10,
    "edged_image_size": 700,
    "blur_radius": 3,
    "canny_coefficients": (0.33, 3),
    "apply_histogram_equalization": False,
    "approx_poly_perimeter_multiplier": 0.02,
    "threshold_sigma_multiplier": 0.01,
    "threshold_offset_multiplier": 0.001,
    "expand_pixels": 1,
    "darkening_percentage": 0.1,
}
