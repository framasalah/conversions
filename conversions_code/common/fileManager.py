import os


def get_number_of_files_in_folder(folder_path):
    file_list = os.listdir(folder_path)
    num_files = len(file_list)
    if ".gitkeep" in file_list:
        num_files -= 1
    if ".directory" in file_list:
        num_files -= 1

    return num_files


def copy_files_to_input_folder(file_paths, input_folder_path):
    for file_path in file_paths:
        filename = file_path.split("/")[-1]
        cmd = "cp " + file_path.replace(" ", "\ ") + " " \
              + input_folder_path.replace(" ", "\ ") + filename.replace(" ", "\ ")
        # print(cmd)
        os.system(cmd)


def delete_all_files_in_folder(folder_path):
    file_list = os.listdir(folder_path)
    for file_name in file_list:
        file_path = folder_path + "/" + file_name
        if os.path.isfile(file_path):
            if file_name != ".gitkeep" and file_name != ".directory":
                os.remove(file_path)
        else:
            print("Could not remove folder {:s}, please remove it manually.".format(file_path))


def list_all_files_with_allowed_extensions(folder_path, allowed_extensions):
    allowed_file_paths = []
    file_list = os.listdir(folder_path)
    for file_name in file_list:
        file_path = folder_path + file_name
        filename, file_extension = os.path.splitext(file_path)
        if file_extension in allowed_extensions:
            # print(file_path)
            allowed_file_paths += [file_path]
    return allowed_file_paths
