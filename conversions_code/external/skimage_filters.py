from scipy import ndimage as ndi
import numpy as np


def threshold_local(image, block_size, offset=0, mode='reflect', cval=0):

    if block_size % 2 == 0:
        raise ValueError("The kwarg ``block_size`` must be odd! Given "
                         "``block_size`` {0} is even.".format(block_size))
    thresh_image = np.zeros(image.shape, 'double')

    sigma = (block_size - 1) / 6.0
    ndi.gaussian_filter(image, sigma, output=thresh_image, mode=mode, cval=cval)

    return thresh_image - offset
