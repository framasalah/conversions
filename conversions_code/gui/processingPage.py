import tkinter as tk


class ProcessingPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        # variable
        self.controller = controller
        self.row_index = 0
        stg = self.controller.settings

        # title
        label = tk.Label(self, text=self.controller.app_layout.title, font=stg["title_font"])
        label.grid(row=self.row_index, columnspan=3)
        self.row_index += 1

        self._insert_empty_row()

        label = tk.Label(self, text="Processing files with the following extension(s):", font=stg["small_title_font"])
        label.grid(row=self.row_index, sticky=tk.W)
        self.row_index += 1

        label = tk.Label(self, text="  " + " ".join(controller.app_layout.allowed_extensions))
        label.config(fg=stg["green_color"], font=stg["small_title_font"])
        label.grid(row=self.row_index, sticky=tk.W)
        self.row_index += 1

        self._insert_empty_row()

        label = tk.Label(self, text="This May take time ...", font=stg["small_title_font"])
        label.grid(row=self.row_index, sticky=tk.W)
        self.row_index += 1

    def _insert_empty_row(self):
        tk.Label(self, text=" " * 5, font=self.controller.settings["base_font"]).grid(row=self.row_index)
        self.row_index += 1
