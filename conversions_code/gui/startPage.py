import tkinter as tk
import os
import subprocess

from conversions_code.common.createToolTip import CreateToolTip
from conversions_code.common import fileManager


class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.row_index = 0
        self.num_columns = 2
        self.grid_columnconfigure(1, weight=0)

        self._add_app_title_and_explanation()

        self._insert_separator()
        self._add_mode1_widget()
        self._insert_separator()
        self._add_mode2_widget()

    def _insert_empty_row(self):
        tk.Label(self, text=" " * 5, font=self.controller.settings["base_font"]).grid(row=self.row_index)
        self.row_index += 1

    def _insert_separator(self):
        stg = self.controller.settings
        w = tk.Canvas(self, width=stg["size"][0], height=stg["size"][1] * 0.002, bg="black")
        w.grid(row=self.row_index, columnspan=self.num_columns)
        self.row_index += 1

    def _add_app_title_and_explanation(self):
        stg = self.controller.settings
        layout = self.controller.app_layout

        # title
        label = tk.Label(self, text=layout.title, font=stg["title_font"])
        label.grid(row=self.row_index, columnspan=self.num_columns)
        self.row_index += 1

        label = tk.Label(self, text="help?", relief='solid', borderwidth=1, font=stg["small_font"])
        CreateToolTip(label, stg, layout.explanation)
        label.grid(row=self.row_index, columnspan=self.num_columns)
        self.row_index += 1
        self._insert_empty_row()

    def _add_mode1_widget(self):
        stg = self.controller.settings
        mode1_explanation_line = "In this mode, all files in the input folder will be processed and \n" \
                                 "the result will be placed in the output folder."
        self._model_title_frame("Mode 1:  ", mode1_explanation_line)
        self._insert_empty_row()

        button = tk.Button(self, text="Process files\nIn input folder", font=stg["small_title_font"],
                           command=lambda: self._proceed_without_file_selection())
        button.config(fg=stg["color1_foreground"], bg=stg["color1_background"])
        button.grid(row=self.row_index, column=0, columnspan=self.num_columns)
        self.row_index += 1

        self.controller.delete_all_inputs_after_processing = tk.IntVar()
        check = tk.Checkbutton(self, text="Delete input files after processing",
                               variable=self.controller.delete_all_inputs_after_processing, font=stg["small_font"])

        check.grid(row=self.row_index, columnspan=self.num_columns)
        self.row_index += 1
        self._insert_empty_row()

        # button input
        button = tk.Button(self, text="Open input folder", font=stg["base_font"],
                           command=lambda: self._open_input_folder())
        button.config(fg=stg["color1_foreground"], bg=stg["color1_background"])
        button.grid(row=self.row_index, column=0, sticky=tk.E)
        CreateToolTip(button, stg, self.controller.paths["input_folder"])

        #  button output
        button = tk.Button(self, text="Open output folder", font=stg["base_font"],
                           command=lambda: self._open_output_folder())
        button.config(fg=stg["color1_foreground"], bg=stg["color1_background"])
        button.grid(row=self.row_index, column=1, sticky=tk.W)
        CreateToolTip(button, stg, self.controller.paths["output_folder"])
        self.row_index += 1
        self._insert_empty_row()

    def _model_title_frame(self, title, explanation):
        stg = self.controller.settings
        mode1_frame = tk.Frame(self)
        label = tk.Label(mode1_frame, text=title, font=stg["small_title_font"])
        label.pack(side="left", fill=None, expand=False)
        label = tk.Label(mode1_frame, text="help?", relief='solid', borderwidth=1, font=stg["small_font"])
        CreateToolTip(label, stg, explanation)
        label.pack(side="left", fill=None, expand=False)
        mode1_frame.grid(row=self.row_index, sticky=tk.W)
        self.row_index += 1

    def _add_mode2_widget(self):
        stg = self.controller.settings

        mode2_explanation_line = "In this mode, the user chooses the file(s) to Process using the gui."
        self._model_title_frame("Mode 2:  ", mode2_explanation_line)

        self.row_index += 1

        button = tk.Button(self, text="Load file(s)\nto process", font=stg["small_title_font"], fg='#0059b3',
                           command=lambda: self._choose_files())
        button.config(fg=stg["color2_foreground"], bg=stg["color2_background"])
        button.grid(row=self.row_index, column=0, columnspan=self.num_columns)
        self.row_index += 1

        button = tk.Button(self, text="  Exit  ", command=self.quit)
        button.config(font=stg["base_font"], fg=stg["red_color"], bg=stg["color2_background"])
        self._insert_empty_row()
        self._insert_empty_row()
        button.grid(row=self.row_index, column=1)

    def _open_input_folder(self):
        os.system("xdg-open " + self.controller.paths["input_folder"])

    def _open_output_folder(self):
        os.system("xdg-open " + self.controller.paths["output_folder"])

    def _choose_files(self):
        cmd = 'zenity --title="file selection" --file-selection --multiple --text "Choose your file(s)" 2> /dev/null'
        result = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True).stdout.decode('utf-8')
        if result is "":
            return
        fileManager.delete_all_files_in_folder(self.controller.paths["input_folder"])
        fileManager.copy_files_to_input_folder((result[:-1]).split("|"), self.controller.paths["input_folder"])

        if not self.controller.app_layout.options:
            self.after(100, self.controller.show_frame("processingPage"))
            self.controller.after(150, lambda: self.controller.process_files())
        else:
            self.controller.show_frame("OptionsPage")

    def _proceed_without_file_selection(self):
        if not self.controller.app_layout.options:
            self.after(100, self.controller.show_frame("processingPage"))
            self.controller.after(150, lambda: self.controller.process_files())
        else:
            self.controller.show_frame("OptionsPage")
