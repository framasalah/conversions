import tkinter as tk
import math
import os
import cv2
from PIL import Image, ImageTk
import numpy as np

from conversions_code.apps import documentScanner


class Point:
    def __init__(self, i_x=0, i_y=0):
        self.x = i_x
        self.y = i_y


class DocumentScannerPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.controller = controller
        self.ds_settings = self.controller.document_scanner_config
        self.bias = 20
        self.row_index = 0
        self.image_idx = 0
        self.single_merged_pdf = True
        self.input_files = []
        self.output_folder = None
        self.images = {}
        self.img_info = {}

        self.canvas = tk.Canvas(self, width=self.controller.settings["size"][1],
                                height=self.controller.settings["size"][1])
        self.canvas.grid(row=0)
        self.image_on_canvas = None

        self._drag_data = {"x": 0, "y": 0, "item": None}

        self.dot_w = self.ds_settings["dot_width_pixels"]
        self.initial_edges = [Point(100, 100), Point(200, 100), Point(100, 200), Point(200, 200)]
        self.ovals = []
        self.lines = []
        self.images_count_txt = None
        self.image_name = None
        self.validate_button = None

        self._draw_buttons()

    def _draw_buttons(self):
        stg = self.controller.settings
        panel = tk.Frame(self)
        # -----+ first line
        self.images_count_txt = tk.Label(panel, text="Image: ", font=stg["title_font"])
        self.images_count_txt.grid(row=0, column=0, sticky=tk.NW)
        tk.Label(panel, text=" " * 5, font=self.controller.settings["base_font"]).grid(row=1, column=0, sticky=tk.NW)
        # -----+ second line
        tk.Label(panel, text="Image Name: ", font=self.controller.settings["base_font"]).grid(row=2,
                                                                                              column=0, sticky=tk.NW)
        self.image_name = tk.Label(panel, text="Image Name: ", font=stg["base_font"])
        self.image_name.grid(row=3, column=0, sticky=tk.NW)
        for i in range(3):
            tk.Label(panel, text=" " * 5, font=self.controller.settings["base_font"]).grid(row=4 + i, column=0)
        # -----+ validate button -----+
        self.validate_button = tk.Button(panel, text="Validate Image\nContours", font=stg["small_title_font"],
                                         command=lambda: self._validate_button_function())
        self.validate_button.config(fg=stg["color1_foreground"], bg=stg["color1_background"])
        self.validate_button.grid(row=7, column=0, sticky=tk.NW)

        tk.Label(panel, text=" " * 5, font=self.controller.settings["base_font"]).grid(row=1)
        panel.grid(row=0, column=1, sticky=tk.N)

    def _update_gui_info(self):
        self.images_count_txt.config(text="Image: {:d}/{:d}".format(self.image_idx + 1, len(self.input_files)))
        file_name = os.path.basename(self.input_files[self.image_idx])
        self.image_name.config(text="- " + file_name)

    def _draw_initial_ovals(self):
        for idx in range(4):
            self.ovals += [self.canvas.create_oval(self.initial_edges[idx].x - self.dot_w,
                                                   self.initial_edges[idx].y - self.dot_w,
                                                   self.initial_edges[idx].x + self.dot_w,
                                                   self.initial_edges[idx].y + self.dot_w,
                                                   outline="red", fill="red", tags=("token",))]

        self.canvas.tag_bind("token", "<ButtonPress-1>", self.drag_start)
        self.canvas.tag_bind("token", "<ButtonRelease-1>", self.drag_stop)
        self.canvas.tag_bind("token", "<B1-Motion>", self.drag)

    def delete_lines(self):
        for idx in range(4):
            self.canvas.delete(self.lines[idx])

    def _draw_lines(self):
        self.lines = []
        points = [self.ovals[0], self.ovals[1], self.ovals[3], self.ovals[2], self.ovals[0]]
        for idx in range(4):
            (x1, y1, _, _) = self.canvas.coords(points[idx])
            (x2, y2, _, _) = self.canvas.coords(points[idx + 1])
            vx = x2 - x1
            vy = y2 - y1
            norm = math.sqrt(vx * vx + vy * vy)
            distance = (self.dot_w + 2) / norm
            self.lines += [self.canvas.create_line(x1 + self.dot_w + vx * distance,
                                                   y1 + self.dot_w + vy * distance,
                                                   x2 + self.dot_w - vx * distance,
                                                   y2 + self.dot_w - vy * distance,
                                                   fill="red", width=3)]

    def drag_start(self, event):
        self._drag_data["item"] = self.canvas.find_closest(event.x, event.y)[0]
        self._drag_data["x"] = event.x
        self._drag_data["y"] = event.y

    def drag_stop(self, event):
        self._drag_data["item"] = None
        self._drag_data["x"] = 0
        self._drag_data["y"] = 0

    def drag(self, event):
        delta_x = event.x - self._drag_data["x"]
        delta_y = event.y - self._drag_data["y"]
        if self._drag_data["item"] != self.image_on_canvas:
            self.canvas.move(self._drag_data["item"], delta_x, delta_y)
        self._drag_data["x"] = event.x
        self._drag_data["y"] = event.y
        self.delete_lines()
        self._draw_lines()

    def _load_image(self):
        input_path = self.input_files[self.image_idx]
        self.images["original_image"] = cv2.imread(input_path)
        self.img_info["original_size"] = (self.images["original_image"].shape[0],
                                          self.images["original_image"].shape[1])
        size = self.img_info["original_size"]
        self.img_info["resize_ratio"] = self.controller.settings["size"][1] / float(max(size[0], size[1]) - self.bias)
        ratio = self.img_info["resize_ratio"]
        # get gray image
        gray_image = cv2.cvtColor(self.images["original_image"], cv2.COLOR_BGR2GRAY)
        # resize image
        self.img_info["displayed_image_size"] = (int(size[1] * ratio), int(size[0] * ratio))
        displayed_image = cv2.resize(gray_image, self.img_info["displayed_image_size"], interpolation=cv2.INTER_AREA)
        self.images["displayed_image"] = ImageTk.PhotoImage(image=Image.fromarray(displayed_image))

        if self.image_idx == 0:
            self.image_on_canvas = self.canvas.create_image(self.bias, self.bias, anchor="nw",
                                                            image=self.images["displayed_image"])
            self._draw_initial_ovals()
            self._draw_lines()
        elif self.image_idx == len(self.input_files):
            self._image_processing_finished()
        else:
            self.canvas.itemconfig(self.image_on_canvas, image=self.images["displayed_image"])
        self._detect_document_edges()

    def _detect_document_edges(self):
        ratio = max(self.img_info["original_size"][0], self.img_info["original_size"][1]) / \
                self.ds_settings["edged_image_size"]
        new_size = (int(self.img_info["original_size"][1] / ratio),
                    int(self.img_info["original_size"][0] / ratio))

        down_sampled_image = cv2.resize(self.images["original_image"], new_size, interpolation=cv2.INTER_AREA)
        blur_radius = self.ds_settings["blur_radius"]

        # documentScanner.show_images({"downsample_edged": down_sampled_image})
        edged_image = documentScanner.edge_detection(down_sampled_image, blur_radius,
                                                     self.ds_settings["canny_coefficients"],
                                                     self.ds_settings["apply_histogram_equalization"])
        roi_dimensions = documentScanner.document_edges(edged_image,
                                                        self.ds_settings["approx_poly_perimeter_multiplier"])

        roi_dimensions = roi_dimensions * ratio
        if roi_dimensions.shape == (4, 1, 2):
            roi_dimensions = self.img_info["resize_ratio"] * roi_dimensions.reshape(4, 2)

            self._move_the_four_corners([Point(roi_dimensions[0, 0], roi_dimensions[0, 1]),
                                         Point(roi_dimensions[1, 0], roi_dimensions[1, 1]),
                                         Point(roi_dimensions[3, 0], roi_dimensions[3, 1]),
                                         Point(roi_dimensions[2, 0], roi_dimensions[2, 1])])
        else:
            self._move_the_four_corners([Point(100, 100), Point(200, 100), Point(100, 200), Point(200, 200)])

    def _move_the_four_corners(self, coordinates):
        for idx in range(4):
            (x, y, _, _) = self.canvas.coords(self.ovals[idx])
            delta_x = coordinates[idx].x - x - self.dot_w + self.bias
            delta_y = coordinates[idx].y - y - self.dot_w + self.bias
            self.canvas.move(self.ovals[idx], delta_x, delta_y)
        self.delete_lines()
        self._draw_lines()

    def _process_current_image(self):
        roi_dimensions = np.zeros((4, 2), dtype=np.float64)
        for idx in range(4):
            (x, y, _, _) = self.canvas.coords(self.ovals[idx])
            roi_dimensions[idx, 0] = abs(x + self.dot_w - self.bias)
            roi_dimensions[idx, 1] = abs(y + self.dot_w - self.bias)
            if roi_dimensions[idx, 0] >= self.img_info["displayed_image_size"][0]:
                roi_dimensions[idx, 0] = self.img_info["displayed_image_size"][0] - 1
            if roi_dimensions[idx, 1] >= self.img_info["displayed_image_size"][1]:
                roi_dimensions[idx, 1] = self.img_info["displayed_image_size"][1] - 1

        roi_dimensions /= self.img_info["resize_ratio"]
        scan = documentScanner.perspective_transform(roi_dimensions, self.images["original_image"])
        # documentScanner.show_images({"scan": scan})
        size = max(self.img_info["original_size"])
        sigma = int(round(size * self.ds_settings["threshold_sigma_multiplier"]))
        offset = int(round(size * self.ds_settings["threshold_offset_multiplier"])) + 7
        expand_pixels = int(self.ds_settings["expand_pixels"]) * 2 + 1
        darkening_percentage = self.ds_settings["darkening_percentage"]
        final_image = documentScanner.post_processing(scan, sigma, offset, expand_pixels, darkening_percentage)
        base_name = os.path.basename(self.input_files[self.image_idx])
        output_path = self.output_folder + base_name
        cv2.imwrite(output_path, final_image)
        if not self.single_merged_pdf:
            cmd = "img2pdf --pagesize A4 {:s} -o {:s}.pdf".format(output_path, self.output_folder + base_name[:-4])
            os.system(cmd)
        print("processed file saved: {0:d}/{1:d} ==> {2:s} (sig:{3:d}, ofs:{4:d}, xpa:{5:d})".
              format(self.image_idx + 1, len(self.input_files), base_name.replace("\ ", " "),
                     sigma, offset, expand_pixels))

    def _validate_button_function(self):
        self._process_current_image()
        self.image_idx += 1
        if self.image_idx == len(self.input_files):
            self._image_processing_finished()
            return
        self._update_gui_info()
        self._load_image()

    def _image_processing_finished(self):
        self.image_idx -= 1
        output_paths = []
        for input_path in self.input_files:
            base_name = os.path.basename(input_path)
            output_paths += [self.output_folder + base_name]

        if self.single_merged_pdf:
            cmd = "img2pdf --pagesize A4 -o {:s}all_images_merged.pdf".format(self.output_folder)
            for path in output_paths:
                cmd += " " + path
            os.system(cmd)
            print("Merged all images into one pdf file")
        print("Deleting intermediate images")
        for output_path in output_paths:
            if os.path.exists(output_path):
                os.remove(output_path)

        self.controller.post_processing_clean_up()

    def conversion_function(self, input_files, output_folder, _options=0):
        self.input_files = input_files
        self.output_folder = output_folder
        if _options == 1:
            self.single_merged_pdf = False
        self._update_gui_info()
        self._load_image()
