import tkinter as tk

from conversions_code.common import fileManager
from conversions_code import config
from conversions_code.gui.startPage import StartPage
from conversions_code.gui.processingPage import ProcessingPage
from conversions_code.gui.optionsPage import OptionsPage
from conversions_code.gui.finalPage import FinalPage
from conversions_code.gui.documentScannerPage import DocumentScannerPage


class PageHandler(tk.Tk):

    def __init__(self, *args, **kwargs):
        # initialization
        self.app_layout = kwargs.get('layout', None)
        kwargs.pop("layout", None)
        tk.Tk.__init__(self, *args, **kwargs)

        # load settings
        self.settings = config.GUI_CONFIG
        self.paths = config.PATHS_CONFIG
        self.document_scanner_config = config.DOCUMENT_SCANNER_CONFIG
        self.selected_files = None
        self.options_choice = -1
        self.frames = {}
        self.delete_all_inputs_after_processing = None

        # container init
        container = tk.Frame(self)

        self.style_frame(container)
        self.set_up_pages(container)
        self.show_frame("StartPage")

    def style_frame(self, container):
        # style controller
        tk.Tk.wm_title(self, self.app_layout.title)
        if not self.settings["enable_resize"]:
            self.resizable(width=tk.FALSE, height=tk.FALSE)
        self.geometry("{:d}x{:d}".format(self.settings["size"][0], self.settings["size"][1]))

        # style parent frame
        self.grid()
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        container.grid(row=0, column=0, sticky=tk.N)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

    def set_up_pages(self, container):
        pages = {
            "StartPage": StartPage,
            "processingPage":  ProcessingPage,
            "OptionsPage": OptionsPage,
            "FinalPage": FinalPage,
            "DocumentScannerPage": DocumentScannerPage,
        }

        for page_name in pages:
            frame = pages[page_name](container, self)
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky="nsew")

    def show_frame(self, cont):
        frame = self.frames[cont]
        self.after(100, frame.tkraise())

    def process_files(self):
        print("\n-----+ Starting conversion process -----+\n")
        print("Deleting previous result")
        fileManager.delete_all_files_in_folder(self.paths["output_folder"])
        print("searching for targets with the following extensions: {:s}".
              format(", ".join(self.app_layout.allowed_extensions)))
        self.selected_files = fileManager.list_all_files_with_allowed_extensions(
            self.paths["input_folder"], self.app_layout.allowed_extensions)
        num = len(self.selected_files)
        if num == 0:
            print("Warning: No target found under the path: " + self.paths["input_folder"])
            self.post_processing_clean_up()
            return
        else:
            print("Number of files found: {:d}".format(num))
        #

        if self.app_layout.special_gui_page is not None:
            self.frames[self.app_layout.special_gui_page].conversion_function(
                self.selected_files, self.paths["output_folder"], self.options_choice)
            self.show_frame(self.app_layout.special_gui_page)
        else:
            self.app_layout.conversion_function(self.selected_files, self.paths["output_folder"], self.options_choice)
            self.post_processing_clean_up()

    def post_processing_clean_up(self):
        self.show_frame("FinalPage")
        # update information about the final page
        files_num = fileManager.get_number_of_files_in_folder(self.paths["input_folder"])
        self.frames["FinalPage"].num_inputs.config(text="     {:d} ".format(files_num))
        files_num = fileManager.get_number_of_files_in_folder(self.paths["output_folder"])
        self.frames["FinalPage"].num_outputs.config(text="     {:d} ".format(files_num))

        # delete all input if user chooses
        if self.delete_all_inputs_after_processing.get() == 1:
            print("Deleting input files")
            fileManager.delete_all_files_in_folder(self.paths["input_folder"])

        print("\n-----+ Conversion process finished -----+\n")
