import tkinter as tk
import os

from conversions_code.common.createToolTip import CreateToolTip


class FinalPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.row_index = 0
        stg = self.controller.settings

        # title
        label = tk.Label(self, text=self.controller.app_layout.title, font=stg["title_font"])
        label.grid(row=self.row_index, columnspan=2)
        self.row_index += 1

        self._insert_empty_row()

        label = tk.Label(self, text="Processing files has finished:", font=stg["small_title_font"])
        label.grid(row=self.row_index, sticky=tk.W)
        self.row_index += 1

        self._insert_empty_row()

        self.num_inputs = self._variable_plus_text(" x", "file(s) were provided as input")

        self.num_outputs = self._variable_plus_text("y ", "file(s) were successfully processed")

        self._insert_empty_row()
        self._insert_empty_row()

        self._open_output_button()

        self._exit_button()

    def _open_output_button(self):
        stg = self.controller.settings
        button = tk.Button(self, text="Open output folder", font=stg["small_title_font"],
                           command=lambda: self._open_output_folder())
        button.config(fg=stg["color1_foreground"], bg=stg["color1_background"])
        button.grid(row=self.row_index, column=0)
        CreateToolTip(button, stg, self.controller.paths["output_folder"])
        self.row_index += 1

    def _exit_button(self):
        stg = self.controller.settings
        button = tk.Button(self, text="  Exit  ", command=self.quit)
        button.config(font=stg["small_title_font"], fg=stg["red_color"], bg=stg["color2_background"])
        self._insert_empty_row()
        self._insert_empty_row()
        button.grid(row=self.row_index, column=1)

    def _open_output_folder(self):
        os.system("xdg-open " + self.controller.paths["output_folder"])

    def _variable_plus_text(self, variable, text):
        stg = self.controller.settings
        mode1_frame = tk.Frame(self)
        variable_ref = tk.Label(mode1_frame, text=variable, font=stg["small_title_font"], fg=stg["green_color"])
        variable_ref.pack(side="left", fill=None, expand=False)
        label = tk.Label(mode1_frame, text=text, font=stg["small_title_font"])
        label.pack(side="left", fill=None, expand=False)
        mode1_frame.grid(row=self.row_index, sticky=tk.W)
        self.row_index += 1
        return variable_ref

    def _insert_empty_row(self):
        tk.Label(self, text=" " * 5, font=self.controller.settings["base_font"]).grid(row=self.row_index)
        self.row_index += 1
