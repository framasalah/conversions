import tkinter as tk


class OptionsPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.row_index = 0
        stg = self.controller.settings

        # title
        label = tk.Label(self, text=self.controller.app_layout.title, font=stg["title_font"])
        label.grid(row=self.row_index, columnspan=2)
        self.row_index += 1

        self._insert_empty_row()

        label = tk.Label(self, text=self.controller.app_layout.options_questions, font=stg["small_title_font"])
        label.grid(row=self.row_index, sticky=tk.W)
        self.row_index += 1

        self._insert_empty_row()

        for i in range(len(self.controller.app_layout.options)):
            button = tk.Button(self, text=self.controller.app_layout.options[i], font=stg["small_title_font"],
                               command=lambda ii=i: self._choose_option(ii))
            button.config(fg=stg["color1_foreground"], bg=stg["color1_background"])
            button.grid(row=self.row_index, column=0, columnspan=2)
            self.row_index += 1
            self._insert_empty_row()

        self._insert_empty_row()
        self._exit_button()

    def _insert_empty_row(self):
        tk.Label(self, text=" " * 5, font=self.controller.settings["base_font"]).grid(row=self.row_index)
        self.row_index += 1

    def _exit_button(self):
        stg = self.controller.settings
        button = tk.Button(self, text="  Exit  ", command=self.quit)
        button.config(font=stg["small_title_font"], fg=stg["red_color"], bg=stg["color2_background"])
        self._insert_empty_row()
        self._insert_empty_row()
        button.grid(row=self.row_index, column=1)

    def _choose_option(self, option_num):
        self.controller.options_choice = option_num
        self.after(100, self.controller.show_frame("processingPage"))
        self.controller.after(150, lambda: self.controller.process_files())
