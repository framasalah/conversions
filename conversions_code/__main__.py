#!/usr/bin/env python3
__version__ = '0.2'
__author__ = 'Salah Eddine Kabbour'

import time
import sys

from conversions_code import gui
from conversions_code.apps import grayPdf
from conversions_code.apps import pngHeatMap
from conversions_code.apps import swapPngJpg
from conversions_code.apps import pngToTxtOcr
from conversions_code.apps import pdfWatermark
from conversions_code.apps import documentScanner


class AppLayout:
    def __init__(self):
        self.title = None
        self.explanation = None
        self.conversion_function = None
        self.options_questions = ""
        self.options = []
        self.allowed_extensions = []
        self.special_gui_page = None


def launch_gui(loaded_app_layout):
    app = gui.PageHandler(layout=loaded_app_layout)
    app.mainloop()


if __name__ == '__main__':

    # app_layout = AppLayout()
    # documentScanner.load_app_layout(app_layout)
    # launch_gui(app_layout)
    # quit()

    if len(sys.argv) != 2:
        print("Wrong number of arguments, please use one of application's shortcuts")
        time.sleep(3)
        quit()

    # print("second argument: ", sys.argv[1])

    app_layout = AppLayout()
    if sys.argv[1] == "pdf_to_gray_pdf":
        grayPdf.load_app_layout(app_layout)
        launch_gui(app_layout)

    elif sys.argv[1] == "png_to_heatmap_png":
        pngHeatMap.load_app_layout(app_layout)
        launch_gui(app_layout)

    elif sys.argv[1] == "swap_png_jpg":
        swapPngJpg.load_app_layout(app_layout)
        launch_gui(app_layout)

    elif sys.argv[1] == "png_to_txt":
        pngToTxtOcr.load_app_layout(app_layout)
        launch_gui(app_layout)

    elif sys.argv[1] == "pdf_to_pdf_with_watermark":
        pdfWatermark.load_app_layout(app_layout)
        launch_gui(app_layout)

    elif sys.argv[1] == "jpg_to_scanned_pdf_document":
        documentScanner.load_app_layout(app_layout)
        launch_gui(app_layout)

    else:
        print("No known app input, please use one of application's shortcuts")
        time.sleep(3)
