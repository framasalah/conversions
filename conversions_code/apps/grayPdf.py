import os


def load_app_layout(layout):
    layout.title = "Colored pdf to gray pdf converter"
    layout.explanation = "Converting colored pdf to gray pdf\n(usually useful for printing without color)"
    layout.conversion_function = conversion_function
    layout.options_questions = ""
    layout.options = []
    layout.allowed_extensions = [".pdf"]


def conversion_function(input_files, output_folder, _options=None):
    list_size = len(input_files)
    for i, file in enumerate(input_files):
        input_path = file.replace(" ", "\ ")
        base_name = os.path.basename(input_path)
        output_path = output_folder + base_name
        cmd = "gs -sOutputFile={:s} -sDEVICE=pdfwrite -sColorConversionStrategy=Gray "\
              "-dProcessColorModel=/DeviceGray -dCompatibiltyLevel=1.4 -dNOPAUSE -dBATCH "\
              "-f {:s} >/dev/null 2>&1".format(output_path, input_path)
        # print(cmd)
        os.system(cmd)
        print("processed file saved: {0:d}/{1:d} ==> {2:s}".format(i + 1, list_size, base_name.replace("\ ", " ")))


