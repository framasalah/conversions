import os


def load_app_layout(layout):
    layout.title = "Swap jpg and png converter"
    layout.explanation = "Converting .png image to .jpg ones,\nand also convert .jpg to .png images."
    layout.conversion_function = conversion_function
    layout.options_questions = ""
    layout.options = []
    layout.allowed_extensions = [".png", ".jpg", ".jpeg"]


def conversion_function(input_files, output_folder, _options=None):
    list_size = len(input_files)
    for i, file in enumerate(input_files):
        input_path = file.replace(" ", "\ ")
        base_name = os.path.basename(input_path)
        filename, file_extension = os.path.splitext(input_path)
        output_path = output_folder + filename.split("/")[-1]
        if file_extension == ".png":
            output_path += ".jpg"
        else:
            output_path += ".png"

        cmd = "convert {:s} {:s}>/dev/null 2>&1".format(input_path, output_path)
        # print(cmd)
        os.system(cmd)
        print("processed file saved: {0:d}/{1:d} ==> {2:s}".format(i + 1, list_size, base_name.replace("\ ", " ")))
