import os


def load_app_layout(layout):
    layout.title = "Png to Txt converter"
    layout.explanation = "Convert images to text using Tesseract, which is an\n" \
                         "open source text recognition engine (OCR)."
    layout.conversion_function = conversion_function
    layout.options_questions = "Choose the language of the text in the image"
    layout.options = ["arabic", "english", "french", "german", "russian"]
    layout.allowed_extensions = [".png"]


def conversion_function(input_files, output_folder, options=0):

    if options == 1:
        lang = "eng"
    elif options == 2:
        lang = "fra"
    elif options == 3:
        lang = "deu"
    elif options == 4:
        lang = "rus"
    else:
        lang = "ara"

    list_size = len(input_files)
    for i, file in enumerate(input_files):
        input_path = file.replace(" ", "\ ")
        base_name = os.path.basename(input_path)
        filename, file_extension = os.path.splitext(input_path)
        output_path = output_folder + filename.split("/")[-1]
        cmd = "tesseract {:s} {:s} -l {:s} >/dev/null 2>&1".format(input_path, output_path, lang)
        # print(cmd)
        os.system(cmd)
        print("processed file saved: {0:d}/{1:d} ==> {2:s}".format(i + 1, list_size, base_name.replace("\ ", " ")))



