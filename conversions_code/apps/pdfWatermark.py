import PyPDF2
import os

from conversions_code import config


def load_app_layout(layout):
    layout.title = "Watermark pdf"
    layout.explanation = "Adding watermark to pdf files. The base watermark pdf path:\n{:s}"\
        .format(config.PATHS_CONFIG["watermark_pdf"])
    layout.conversion_function = conversion_function
    layout.options_questions = ""
    layout.options = []
    layout.allowed_extensions = [".pdf"]


def conversion_function(input_files, output_folder, _options=None):
    watermark_file = config.PATHS_CONFIG["watermark_pdf"]
    if not os.path.isfile(watermark_file):
        print("Warning: No watermark file found under: {:s}".format(watermark_file))
        return
    list_size = len(input_files)

    # process each file in this loop
    for i, file in enumerate(input_files):
        input_path = file.replace(" ", "\ ")
        base_name = os.path.basename(input_path)
        output_path = output_folder + base_name
        with open(input_path, "rb") as file_handle_input:
            # read content of the original file
            pdf = PyPDF2.PdfFileReader(file_handle_input)

            with open(watermark_file, "rb") as file_handle_watermark:
                # read content of the watermark
                watermark = PyPDF2.PdfFileReader(file_handle_watermark)

                # get first page of the watermark PDF
                first_page_watermark = watermark.getPage(0)

                # create a pdf writer object for the output file
                pdf_writer = PyPDF2.PdfFileWriter()

                num_pages = pdf.getNumPages()
                for p_num in range(num_pages):
                    # get a page of the original PDF
                    page = pdf.getPage(p_num)

                    # merge the two pages
                    page.mergePage(first_page_watermark)

                    # add page
                    pdf_writer.addPage(page)

                with open(output_path, "wb") as file_handle_output:
                    # write the watermarked file to the new file
                    pdf_writer.write(file_handle_output)
                    
        print("processed file saved: {0:d}/{1:d} ==> {2:s}".format(i + 1, list_size, base_name.replace("\ ", " ")))
