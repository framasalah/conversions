import math
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def load_app_layout(layout):
    layout.title = "Png to heat-map-png converter"
    layout.explanation = "Convert regular gray .png images to heat-maps, this is used usually in order to\n" \
                         "make depth maps captured by rgd-d cameras easier to understand"
    layout.conversion_function = conversion_function
    layout.options_questions = "The toolbar is an indicator of image pixel levels"
    layout.options = ["Display toolbar", "Do not display toolbar"]
    layout.allowed_extensions = [".png"]


def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.2989, 0.5870, 0.1140])


def conversion_function(input_files, output_folder, options=0):
    list_size = len(input_files)
    for i, file in enumerate(input_files):
        input_path = file.replace(" ", "\ ")
        base_name = os.path.basename(input_path)
        output_path = output_folder + base_name

        img = mpimg.imread(input_path)
        if len(img.shape) == 3:
            img = rgb2gray(img)

        img255 = img * 255.0
        x_size, y_size = math.floor(img.shape[1]/50), math.floor(img.shape[0]/60)
        fig = plt.figure(figsize=(x_size, y_size), dpi=120)
        plt.imshow(img255, cmap="hot")
        plt.colorbar()

        plt.imshow(img, cmap="hot")

        rect = fig.patch
        rect.set_facecolor((.9, .9, .9))
        if options == 0:
            plt.savefig(output_path, facecolor=fig.get_facecolor(), edgecolor='none', bbox_inches="tight")
        else:
            plt.imsave(output_path, img, cmap="hot")
        print("processed file saved: {0:d}/{1:d} ==> {2:s}".format(i + 1, list_size, base_name.replace("\ ", " ")))



