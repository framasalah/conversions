import cv2
import numpy as np
from scipy import signal


from conversions_code.external import imutils2
from conversions_code.external import skimage_filters


def load_app_layout(layout):
    layout.title = "Document Scanner"
    layout.explanation = "Transform jpg images captured by a camera to \na scanned and digitized paper documents"
    layout.conversion_function = None
    layout.options_questions = "Merge all provided input images to a single file," \
                               "\nor output an individual pdf for each jpg file?"
    layout.options = ["Single merged document", "individual documents"]
    layout.allowed_extensions = [".jpg"]
    layout.special_gui_page = "DocumentScannerPage"


def show_images(images_dict):
    for title in images_dict:
        cv2.imshow(title, images_dict[title])
    cv2.waitKey(0)  # press 0 to close all cv2 windows
    cv2.destroyAllWindows()


def edge_detection(i_image, i_blur_radius=3, i_canny_coefs=(0.33, 3), equalize_histogram=True):
    # convert image to gray scale. This will remove any color noise
    gray_image = cv2.cvtColor(i_image, cv2.COLOR_BGR2GRAY)  # blur the image to remove high frequency noise
    if equalize_histogram:
        gray_image = cv2.equalizeHist(gray_image)
    if i_blur_radius > 1:
        gray_image = cv2.blur(gray_image, (i_blur_radius, i_blur_radius))  # then we performed canny edge detection
    v = np.median(gray_image)
    # apply automatic Canny edge detection using the computed median
    sigma = i_canny_coefs[0]
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged_image = cv2.Canny(gray_image, lower, upper, i_canny_coefs[1])
    # show the gray and edge-detected image
    # show_images({"gray": gray_image,  "Edge Detected Image": edged_image})
    return edged_image


def document_edges(i_edged_image, i_approx_poly_perimeter_multiplier=0.02):
    # find the contours in the edged image, sort area wise
    # keeping only the largest ones
    all_contours = cv2.findContours(i_edged_image.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    all_contours = imutils2.grab_contours(all_contours)  # descending sort contours area and keep top 1
    all_contours = sorted(all_contours, key=cv2.contourArea, reverse=True)[:1]  # approximate the contour
    if not all_contours:
        return np.zeros((2, 1))
    perimeter = cv2.arcLength(all_contours[0], True)
    roi_dimensions = cv2.approxPolyDP(all_contours[0], i_approx_poly_perimeter_multiplier * perimeter, True)
    # show the contour on image
    # cv2.drawContours(i_image, [roi_dimensions], -1, (0, 255, 0), 2)
    # _show_images({'contour outline': i_image})
    return roi_dimensions


def perspective_transform(i_roi_dimensions, i_original_image):
    # reshape coordinates array
    i_roi_dimensions = i_roi_dimensions.reshape(4, 2)  # list to hold ROI coordinates
    rect = np.zeros((4, 2), dtype="float32")  # top left corner will have the smallest sum,
    # bottom right corner will have the largest sum
    s = np.sum(i_roi_dimensions, axis=1)
    rect[0] = i_roi_dimensions[np.argmin(s)]
    rect[2] = i_roi_dimensions[np.argmax(s)]  # top-right will have smallest difference
    # botton left will have largest difference
    diff = np.diff(i_roi_dimensions, axis=1)
    rect[1] = i_roi_dimensions[np.argmin(diff)]
    rect[3] = i_roi_dimensions[np.argmax(diff)]  # top-left, top-right, bottom-right, bottom-left
    tl, tr, br, bl = rect  # compute width of ROI
    width_a = np.sqrt((tl[0] - tr[0]) ** 2 + (tl[1] - tr[1]) ** 2)
    width_b = np.sqrt((bl[0] - br[0]) ** 2 + (bl[1] - br[1]) ** 2)
    max_width = max(int(width_a), int(width_b))  # compute height of ROI
    height_a = np.sqrt((tl[0] - bl[0]) ** 2 + (tl[1] - bl[1]) ** 2)
    height_b = np.sqrt((tr[0] - br[0]) ** 2 + (tr[1] - br[1]) ** 2)
    max_height = max(int(height_a), int(height_b))

    # Set of destinations points for "birds eye view"
    # dimension of the new image
    dst = np.array([
        [0, 0],
        [max_width - 1, 0],
        [max_width - 1, max_height - 1],
        [0, max_height - 1]], dtype="float32")  # compute the perspective transform matrix and then apply it
    transform_matrix = cv2.getPerspectiveTransform(rect, dst)  # transform ROI
    # lets see the wraped document
    scan = cv2.warpPerspective(i_original_image, transform_matrix, (max_width, max_height))
    # _show_images({"Scaned": scan})
    return scan


def expand_indices(i_indices, i_expand_radius=3):
    weights = np.ones((i_expand_radius, i_expand_radius))
    new_indices = np.invert(signal.convolve2d(np.invert(i_indices), weights, 'same').astype(np.bool))
    return new_indices


def post_processing(i_scan, i_sigma=9, i_offset=8, i_expand_radius=3, i_darkening_percentage=0.1):

    scan_gray = cv2.cvtColor(i_scan, cv2.COLOR_BGR2GRAY)  # display final gray image
    threshold = skimage_filters.threshold_local(scan_gray, i_sigma, offset=i_offset)
    filtered = i_scan
    for i in range(3):
        threshold_indices = scan_gray > threshold
        if i_expand_radius > 1:
            threshold_indices = expand_indices(threshold_indices, i_expand_radius)
        tmp = i_scan[:, :, i] / (1 + i_darkening_percentage)
        tmp[threshold_indices] = 255
        filtered[:, :, i] = tmp

    # _show_images({"filtered": filtered})
    return filtered
